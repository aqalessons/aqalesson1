import pytest
import requests


def pytest_addoption(parser):
    parser.addoption(
        "--url",
        default="https://restful-booker.herokuapp.com",
        help="This is baseURL",
        required=False
    )

    parser.addoption(
        "--method",
        default="get",
        choices=["get", "post", "put", "patch", "delete"],
        help="Method to execute"
    )


@pytest.fixture()
def url_param(request):
    return request.config.getoption("--url")


@pytest.fixture
def request_method(request):
    return getattr(requests, request.config.getoption("--method"))




