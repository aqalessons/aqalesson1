import requests
import pytest
from jsonschema import validate



def pytest_namespace():
    return {'current_id': None, 'token': None}

booking_ids_schema = {
    "type": "array",
    "properties": {
        "object": {
            "type": "object",
            "properties": {
                "bookingid": {"type": "number"}
            }
        }
    },
    "required": ["object", "bookingid"]
}

schema = {
    "type": "object",
    "properties": {
        "firstname": {"type": "string"},
        "lastname": {"type": "string"},
        "totalprice": {"type": "number"},
        "depositpaid": {"type": "boolean"},
        "bookingdates": {
            "type": "object",
            "properties": {
                "checkin": {"type": "string", "format": "date"},
                "checkout": {"type": "string", "format": "date"}
            },
        },
        "additionalneeds": {"type": "string"}
    },
    "required": ["firstname", "lastname", "totalprice", "depositpaid", "bookingdates", "additionalneeds"]
}

schema_with_id = {
    "type": "object",
    "properties": {
        "bookingid": {"type": "number"},
        "booking": {
            "type": "object",
            "properties": {
                "firstname": {"type": "string"},
                "lastname": {"type": "string"},
                "totalprice": {"type": "number"},
                "depositpaid": {"type": "boolean"},
                "bookingdates": {
                    "type": "object",
                    "properties": {
                        "checkin": {"type": "string", "format": "date"},
                        "checkout": {"type": "string", "format": "date"}
                    },
                },
                "additionalneeds": {"type": "string"}
            }

        }
    },
    "required": ["bookingid", "booking"]
}


def test_get_all_ids(url_param, request_method):
    url = f"{url_param}/booking"
    response = requests.get(url=url)
    response_body = response.json()
    assert response.status_code == 200
    validate(instance=response_body, schema=booking_ids_schema)


def test_ping(url_param, request_method):
    url = f"{url_param}/ping"
    response = requests.get(url=url)
    assert response.status_code == 201
    print(response.text)


@pytest.mark.dependency()
def test_create_booking(url_param, request_method):
    user = {
        "firstname": "Michael",
        "lastname": "Wright",
        "totalprice": 777,
        "depositpaid": False,
        "bookingdates": {
            "checkin": "2022-02-03",
            "checkout": "2023-03-02"
        },
        "additionalneeds": "Some oranges"
    }
    url = f"{url_param}/booking"
    response = requests.post(url=url, json=user)
    id = response.json()
    current_id = id.get('bookingid')
    pytest.current_id = current_id
    assert response.status_code == 200, "Wrong status"
    validate(instance=id, schema=schema_with_id)


def test_filter_by_name(url_param, request_method):
    url = f"{url_param}/booking?firstname=Michael&lastname=Wright"
    response = requests.get(url=url)
    response_body = response.json()
    assert response.status_code == 200
    validate(instance=response_body, schema=booking_ids_schema)


def test_filter_by_date(url_param, request_method):
    url = f"{url_param}/booking?checkin=2022-02-03&checkout=2023-03-02"
    response = requests.get(url=url)
    response_body = response.json()
    assert response.status_code == 200
    validate(instance=response_body, schema=booking_ids_schema)


@pytest.mark.dependency()
def test_booking():
    user = {
        "username": "admin",
        "password": "password123"
    }
    response = requests.post(url="https://restful-booker.herokuapp.com/auth", json=user)
    token = response.json()
    token = token.get('token')
    pytest.token = token
    print(response.json())


@pytest.mark.dependency(depends=["test_create_booking"])
def test_get_by_id(url_param, request_method):
    url = f"{url_param}/booking/{pytest.current_id}"
    response = requests.get(url=url)
    response_body = response.json()
    validate(instance=response_body, schema=schema)
    assert response.status_code == 200
    assert response_body.get("firstname") == "Michael", "Wrong name in response"
    assert response_body.get("lastname") == "Wright", "Wrong lastname in response"
    assert response_body.get("totalprice") == 777, "Wrong price in response"
    assert response_body.get("depositpaid") is False, "Deposit is paid in response"


@pytest.mark.dependency(depends=["test_create_booking", "test_booking"])
def test_update_booking_put(url_param, request_method):
    user = {
        "firstname": "Harriet",
        "lastname": "Lovely",
        "totalprice": 277,
        "depositpaid": True,
        "bookingdates": {
            "checkin": "2023-02-03",
            "checkout": "2024-03-02"
        },
        "additionalneeds": "Some apples"
    }
    cookie = {
        'token': f'{pytest.token}'
    }
    url = f"{url_param}/booking/{pytest.current_id}"
    response = requests.put(url=url, json=user, cookies=cookie)
    response_body = response.json()
    validate(instance=response_body, schema=schema)
    assert response.status_code == 200
    assert response_body.get("firstname") == "Harriet", "Wrong name updated(PUT)"
    assert response_body.get("lastname") == "Lovely", "Wrong lastname updated(PUT)"
    assert response_body.get("totalprice") == 277, "Price updated wrong(PUT)"
    assert response_body.get("depositpaid") is True, "Deposit is still not paid(PUT)"


@pytest.mark.dependency(depends=["test_create_booking", "test_booking"])
def test_update_booking_patch(url_param, request_method):
    user = {
        "firstname": "James",
        "lastname": "Hewitt"
    }
    cookie = {
        'token': f'{pytest.token}'
    }
    url = f"{url_param}/booking/{pytest.current_id}"
    response = requests.patch(url=url, json=user, cookies=cookie)
    response_body = response.json()
    validate(instance=response_body, schema=schema)
    assert response.status_code == 200
    assert response_body.get("firstname") == "James", "Wrong name updated(PATCH)"
    assert response_body.get("lastname") == "Hewitt", "Wrong lastname updated(PATCH)"


@pytest.mark.dependency(depends=["test_create_booking", "test_booking"])
def test_delete_by_id(url_param, request_method):
    cookie = {
        'token': f'{pytest.token}'
    }
    url = f"{url_param}/booking/{pytest.current_id}"
    response = requests.delete(url=url, cookies=cookie)
    assert response.status_code == 201, "Booking is not deleted"

