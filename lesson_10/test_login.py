from selenium.webdriver.common.by import By


class TestLoginPositive:
    def test_login_standard_user(self, browser):
        user = {
            'username': 'standard_user',
            'password': 'secret_sauce'
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/inventory.html'

    def test_login_problem_user(self, browser):
        user = {
            'username': 'problem_user',
            'password': 'secret_sauce'
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/inventory.html'

    def test_login_performance_glitch_user(self, browser):
        user = {
            'username': 'performance_glitch_user',
            'password': 'secret_sauce'
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/inventory.html'

    def test_login_error_user(self, browser):
        user = {
            'username': 'error_user',
            'password': 'secret_sauce'
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/inventory.html'

    def test_login_visual_user(self, browser):
        user = {
            'username': 'visual_user',
            'password': 'secret_sauce'
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/inventory.html'


class TestLoginNegative:
    def test_login_locked_out_user(self, browser):
        user = {
            'username': 'locked_out_user',
            'password': 'secret_sauce'
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Sorry, this user has been locked out."

    def test_login_username_empty(self, browser):
        user = {
            'username': 'standard_user',
            'password': 'secret_sauce'
        }
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username is required"

    def test_login_password_empty(self, browser):
        user = {
            'username': 'standard_user',
            'password': 'secret_sauce'
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Password is required"

    def test_login_username_and_password_empty(self, browser):
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username is required"

    def test_login_username_not_valid(self, browser):
        user = {
            'username': 'Oh_hi_Mark_user',
            'password': 'secret_sauce'
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_password_not_valid_standard(self, browser):
        user = {
            'username': 'standard_user',
            'password': '123456'
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_password_not_valid_problem_user(self, browser):
        user = {
            'username': 'problem_user',
            'password': '123456'
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_password_not_valid_locked_out_user(self, browser):
        user = {
            'username': 'locked_out_user',
            'password': '123456'
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_password_not_valid_performance_glitch_user(self, browser):
        user = {
            'username': 'performance_glitch_user',
            'password': '123456'
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_password_not_valid_error_user(self, browser):
        user = {
            'username': 'error_user',
            'password': '123456'
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_password_not_valid_visual_user(self, browser):
        user = {
            'username': 'visual_user',
            'password': '123456'
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_first_space_in_password_standard_user(self, browser):
        user = {
            'username': 'standard_user',
            'password': ' secret_sauce'
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_first_space_password_problem_user(self, browser):
        user = {
            'username': 'problem_user',
            'password': ' secret_sauce'
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_first_space_in_password_locked_out_user(self, browser):
        user = {
            'username': 'locked_out_user',
            'password': ' secret_sauce'
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_first_space_in_password_performance_glitch_user(self, browser):
        user = {
            'username': 'performance_glitch_user',
            'password': ' secret_sauce'
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_first_space_in_password_error_user(self, browser):
        user = {
            'username': 'error_user',
            'password': ' secret_sauce'
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_first_space_in_password_visual_user(self, browser):
        user = {
            'username': 'visual_user',
            'password': ' secret_sauce'
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_last_space_in_password_standard_user(self, browser):
        user = {
            'username': 'standard_user',
            'password': 'secret_sauce '
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_last_space_in_password_problem_user(self, browser):
        user = {
            'username': 'problem_user',
            'password': 'secret_sauce '
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_last_space_in_password_locked_out_user(self, browser):
        user = {
            'username': 'locked_out_user',
            'password': 'secret_sauce '
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_last_space_in_password_performance_glitch_user(self, browser):
        user = {
            'username': 'performance_glitch_user',
            'password': 'secret_sauce '
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_last_space_in_password_error_user(self, browser):
        user = {
            'username': 'error_user',
            'password': 'secret_sauce '
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_last_space_in_password_visual_user(self, browser):
        user = {
            'username': 'visual_user',
            'password': 'secret_sauce '
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_username_first_capital_letter(self, browser):
        user = {
            'username': 'Standard_user',
            'password': 'secret_sauce '
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_username_all_capital_letters(self, browser):
        user = {
            'username': 'STANDARD_USER',
            'password': 'secret_sauce '
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_username_camel_case(self, browser):
        user = {
            'username': 'Standard_User',
            'password': 'secret_sauce '
        }
        browser.find_element('css selector', "input[data-test='username']").send_keys(user['username'])
        browser.find_element(By.CSS_SELECTOR, '#password').send_keys(user['password'])
        browser.find_element(By.CSS_SELECTOR, '#login-button').click()

        assert browser.current_url == 'https://www.saucedemo.com/'
        assert browser.find_element("xpath", "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"
