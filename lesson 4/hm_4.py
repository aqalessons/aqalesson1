# Задание 1

def convert(C):
    F = (C*9 / 5) + 32
    print(int(F))
    
    
convert(23)

# Задание 2

def validator():
    number = input("Введите число: ")
    if len(number) == 0:
        print('Вы ввели пустое значение')
    elif number[0] != '-' and not number.isdigit():
        print('Вы ввели не число')
    else:
        number = int(number)
        if number > 0:
            print('Вы ввели положительное число')
        elif number < 0:
            print('Вы ввели отрицательное число')
        elif number == 0:
            print('Вы ввели ноль')
   
    
validator()

# Задание 3

def exponent():
    base = int(input('Введите число: '))
    exp = int(input("Введите экспоненту: "))
    result = base ** exp
    print(result)
    
exponent()