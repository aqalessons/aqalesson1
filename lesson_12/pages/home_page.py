from lesson_12.pages.base_page import BasePage
from lesson_12.locators import Home

from selenium.webdriver.common.by import By


class LoginPage(BasePage):
    def login(self, username, password):
        self._input(Home.LoginForm.username_input, username)
        self._input(Home.LoginForm.password_input, password)
        self._click(Home.LoginForm.login_button)

    def get_error_text(self):
        return self._get_text(Home.LoginForm.error_text)





