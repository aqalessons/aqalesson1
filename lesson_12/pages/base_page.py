import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:
    def __init__(self, driver: webdriver):
        self.driver = driver
        self.base_url = "https://www.saucedemo.com/"

    def open(self, url: str = ""):
        self.driver.get(self.base_url + url)

    def __element(self, locator: dict, index: int, link_text: str = None):
        by = None
        if link_text:
            by = By.LINK_TEXT
        elif 'css' in locator.keys():
            by = By.CSS_SELECTOR
            locator = locator['css']
        elif 'id' in locator.keys():
            by = By.ID
            locator = locator['id']
        elif 'xpath' in locator.keys():
            by = By.XPATH
            locator = locator['xpath']
        return self.driver.find_elements(by, locator)[index]

    def _click(self, locator, index=0):
        self.__element(locator, index).click()

    def _input(self, locator, value, index=0):
        element = self.__element(locator, index)
        element.clear()
        element.send_keys(value)

    def _get_text(self, locator, index=0):
        return self.__element(locator, index).text




