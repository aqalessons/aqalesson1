import pytest
from selenium import webdriver
from lesson_12.pages.home_page import LoginPage


class TestLoginPositive:

    def test_login_standard_user(self, driver):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.login("standard_user", "secret_sauce")

        assert driver.current_url == 'https://www.saucedemo.com/inventory.html'

    def test_login_problem_user(self, driver):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.login("problem_user", "secret_sauce")

        assert driver.current_url == 'https://www.saucedemo.com/inventory.html'

    def test_login_performance_glitch_user(self, driver):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.login("performance_glitch_user", "secret_sauce")

        assert driver.current_url == 'https://www.saucedemo.com/inventory.html'

    def test_login_error_user(self, driver):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.login("error_user", "secret_sauce")

        assert driver.current_url == 'https://www.saucedemo.com/inventory.html'

    def test_login_visual_user(self, driver):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.login("visual_user", "secret_sauce")

        assert driver.current_url == 'https://www.saucedemo.com/inventory.html'


class TestLoginNegative:
    def test_enter_without_login(self, driver):
        login_page = LoginPage(driver)
        login_page.open("inventory.html")

        assert driver.current_url == 'https://www.saucedemo.com/'
        assert login_page.get_error_text() == "Epic sadface: You can only access '/inventory.html' when you are logged in."

    def test_login_locked_out_user(self, driver):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.login("locked_out_user", "secret_sauce")

        assert driver.current_url == 'https://www.saucedemo.com/'
        assert login_page.get_error_text() == "Epic sadface: Sorry, this user has been locked out."

    def test_login_username_empty(self, driver: webdriver):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.login("", "secret_sauce")

        assert driver.current_url == 'https://www.saucedemo.com/'
        assert login_page.get_error_text() == "Epic sadface: Username is required"

    def test_login_password_empty(self, driver: webdriver):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.login('standard_user', "")

        assert driver.current_url == 'https://www.saucedemo.com/'
        assert login_page.get_error_text() == "Epic sadface: Password is required"

    def test_login_username_and_password_empty(self, driver: webdriver):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.login("", "")

        assert driver.current_url == 'https://www.saucedemo.com/'
        assert login_page.get_error_text() == "Epic sadface: Username is required"

    def test_login_username_not_valid(self, driver: webdriver):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.login('Oh_hi_Mark_user', "secret_sauce")

        assert driver.current_url == 'https://www.saucedemo.com/'
        assert login_page.get_error_text() == "Epic sadface: Username and password do not match any user in this service"

    def test_login_password_not_valid(self, driver: webdriver):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.login('standard_user', "123456")

        assert driver.current_url == 'https://www.saucedemo.com/'
        assert login_page.get_error_text() == "Epic sadface: Username and password do not match any user in this service"

    def test_login_first_space_in_password(self, driver: webdriver):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.login('standard_user', " secret_sauce")

        assert driver.current_url == 'https://www.saucedemo.com/'
        assert login_page.get_error_text() == "Epic sadface: Username and password do not match any user in this service"

    def test_login_last_space_in_password(self, driver: webdriver):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.login('standard_user', "secret_sauce ")

        assert driver.current_url == 'https://www.saucedemo.com/'
        assert login_page.get_error_text() == "Epic sadface: Username and password do not match any user in this service"

    def test_login_first_capital_letter_in_username(self, driver: webdriver):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.login('Standard_user', "secret_sauce")

        assert driver.current_url == 'https://www.saucedemo.com/'
        assert login_page.get_error_text() == "Epic sadface: Username and password do not match any user in this service"

    def test_login_all_capital_letters_in_username(self, driver: webdriver):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.login('STANDARD_USER', "secret_sauce")

        assert driver.current_url == 'https://www.saucedemo.com/'
        assert login_page.get_error_text() == "Epic sadface: Username and password do not match any user in this service"

    def test_login_camel_case_in_username(self, driver: webdriver):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.login('Standard_User', "secret_sauce")

        assert driver.current_url == 'https://www.saucedemo.com/'
        assert login_page.get_error_text() == "Epic sadface: Username and password do not match any user in this service"

    def test_login_with_username_and_password_russian_letters(self, driver: webdriver):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.login("ыефтвфкв_гыук", "ыускуе_ыфгсу")

        assert driver.current_url == 'https://www.saucedemo.com/'
        assert login_page.get_error_text() == "Epic sadface: Username and password do not match any user in this service"

    def test_login_with_username_russian_letters(self, driver: webdriver):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.login("ыефтвфкв_гыук", "secret_sauce")

        assert driver.current_url == 'https://www.saucedemo.com/'
        assert login_page.get_error_text() == "Epic sadface: Username and password do not match any user in this service"

    def test_login_with_password_russian_letters(self, driver: webdriver):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.login("standard_user", "ыускуе_ыфгсу")

        assert driver.current_url == 'https://www.saucedemo.com/'
        assert login_page.get_error_text() == "Epic sadface: Username and password do not match any user in this service"