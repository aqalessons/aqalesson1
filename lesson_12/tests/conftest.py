import pytest
from selenium import webdriver


@pytest.fixture()
def driver(request):
    options = webdriver.ChromeOptions()
    options.add_argument("--headless")
    driver = webdriver.Chrome(options=options)
    # driver = webdriver.Chrome()
    driver.maximize_window()

    def teardown():
        driver.quit()

    request.addfinalizer(teardown)

    return driver
