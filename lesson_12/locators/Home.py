class Home:

    class LoginForm:
        username_input = {"id": "user-name"}
        password_input = {"id": "password"}
        login_button = {"id": "login-button"}
        error_text = {"xpath": "//h3[@data-test='error']"}
