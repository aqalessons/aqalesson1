import pytest
from selenium import webdriver
from lesson_11.pages.home_page import LoginPage
from selenium.webdriver.common.by import By
import time

class TestLoginPositive:
    def test_login_standard_user(self, driver: webdriver):
        home_page = LoginPage(driver)
        home_page.login("standard_user", "secret_sauce")
        assert driver.current_url == 'https://www.saucedemo.com/inventory.html'

    def test_login_problem_user(self, driver: webdriver):
        home_page = LoginPage(driver)
        home_page.login("problem_user", "secret_sauce")
        assert driver.current_url == 'https://www.saucedemo.com/inventory.html'

    def test_login_performance_glitch_user(self, driver: webdriver):
        home_page = LoginPage(driver)
        home_page.login("performance_glitch_user", "secret_sauce")
        assert driver.current_url == 'https://www.saucedemo.com/inventory.html'

    def test_login_error_user(self, driver: webdriver):
        home_page = LoginPage(driver)
        home_page.login("error_user", "secret_sauce")
        assert driver.current_url == 'https://www.saucedemo.com/inventory.html'

    def test_login_visual_user(self, driver: webdriver):
        home_page = LoginPage(driver)
        home_page.login("visual_user", "secret_sauce")
        assert driver.current_url == 'https://www.saucedemo.com/inventory.html'


class TestLoginNegative:
    def test_login_locked_out_user(self, driver: webdriver):
        home_page = LoginPage(driver)
        home_page.login("locked_out_user", "secret_sauce")
        assert driver.current_url == 'https://www.saucedemo.com/'
        assert driver.find_element(By.XPATH, "//h3[@data-test='error']").text == "Epic sadface: Sorry, this user has been locked out."

    def test_login_username_empty(self, driver: webdriver):
        home_page = LoginPage(driver)
        home_page.login("", "secret_sauce")
        assert driver.current_url == 'https://www.saucedemo.com/'
        assert driver.find_element(By.XPATH, "//h3[@data-test='error']").text == "Epic sadface: Username is required"

    def test_login_password_empty(self, driver: webdriver):
        home_page = LoginPage(driver)
        home_page.login('standard_user', "")
        assert driver.current_url == 'https://www.saucedemo.com/'
        assert driver.find_element(By.XPATH, "//h3[@data-test='error']").text == "Epic sadface: Password is required"

    def test_login_username_and_password_empty(self, driver: webdriver):
        home_page = LoginPage(driver)
        home_page.login("", "")
        assert driver.current_url == 'https://www.saucedemo.com/'
        assert driver.find_element(By.XPATH, "//h3[@data-test='error']").text == "Epic sadface: Username is required"

    def test_login_username_not_valid(self, driver: webdriver):
        home_page = LoginPage(driver)
        home_page.login('Oh_hi_Mark_user', "secret_sauce")
        assert driver.current_url == 'https://www.saucedemo.com/'
        assert driver.find_element(By.XPATH, "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_password_not_valid(self, driver: webdriver):
        home_page = LoginPage(driver)
        home_page.login('standard_user', "123456")
        assert driver.current_url == 'https://www.saucedemo.com/'
        assert driver.find_element(By.XPATH, "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_first_space_in_password(self, driver: webdriver):
        home_page = LoginPage(driver)
        home_page.login('standard_user', " secret_sauce")
        assert driver.current_url == 'https://www.saucedemo.com/'
        assert driver.find_element(By.XPATH, "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_last_space_in_password(self, driver: webdriver):
        home_page = LoginPage(driver)
        home_page.login('standard_user', "secret_sauce ")
        assert driver.current_url == 'https://www.saucedemo.com/'
        assert driver.find_element(By.XPATH, "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_first_capital_letter_in_username(self, driver: webdriver):
        home_page = LoginPage(driver)
        home_page.login('Standard_user', "secret_sauce")
        assert driver.current_url == 'https://www.saucedemo.com/'
        assert driver.find_element(By.XPATH, "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_all_capital_letters_in_username(self, driver: webdriver):
        home_page = LoginPage(driver)
        home_page.login('STANDARD_USER', "secret_sauce")
        assert driver.current_url == 'https://www.saucedemo.com/'
        assert driver.find_element(By.XPATH, "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"

    def test_login_camel_case_in_username(self, driver: webdriver):
        home_page = LoginPage(driver)
        home_page.login('Standard_User', "secret_sauce")
        assert driver.current_url == 'https://www.saucedemo.com/'
        assert driver.find_element(By.XPATH, "//h3[@data-test='error']").text == "Epic sadface: Username and password do not match any user in this service"