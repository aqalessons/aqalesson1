import pytest
from selenium import webdriver
from selenium.webdriver import ChromeOptions


@pytest.fixture()
def driver(request):
    options = webdriver.ChromeOptions()
    options.add_argument("--headless")
    driver = webdriver.Chrome(options=options)
    # driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.saucedemo.com/")

    def teardown():
        driver.quit()

    request.addfinalizer(teardown)

    return driver
