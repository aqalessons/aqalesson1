from selenium import webdriver


class BasePage:
    def __init__(self, driver: webdriver):
        self.driver = driver
        self.base_url = "https://www.saucedemo.com"

    def open(self, url: str = ""):
        self.driver.get(self.base_url + url)
