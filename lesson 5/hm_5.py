import random
# Задание 1:
class Calculator:
    def read(self):
        self.first_number = int(input('Введите первое число: '))
        self.second_number = int(input('Введите второе число: '))
        return self.first_number, self.second_number

    def sum(self):
        print(self.first_number + self.second_number)

    def multiply(self):
        print(self.first_number * self.second_number)

    def difference(self):
        print(self.first_number - self.second_number)

    def division(self):
          self.div = self.first_number / self.second_number
          print(round(self.div, 1))

    def modulo(self):
        print(self.first_number % self.second_number)


calculator = Calculator()

calculator.read()
calculator.sum()
calculator.multiply()
calculator.difference()
calculator.division()
calculator.modulo()

# Задание 2:

class Circle:

    def read(self):
        self.radius = int(input("Введите радиус круга: "))
        return self.radius

    def get_s_of_circle(self):
        self.S = 3.14 * self.radius
        print(round(self.S, 1))

    def get_circumference(self):
        self.C = 2 * 3.14 * self.radius
        print(round(self.C, 1))

    def get_radius(self):
        print(self.radius)


circle = Circle()
circle.read()
circle.get_radius()
circle.get_s_of_circle()
circle.get_circumference()


# Задание 3:

class Host:

     def __init__(self):
         self.guests = []
         self.guestList = []
         self.guestRejected = []

     def add_guests(self):
         number = int(input('Enter number of guests: '))
         for k in range(number):
             self.guests.append(input('Enter guest name: '))
         self.guestsSTR = ', '.join(self.guests)
         return self.guests


     def setGuestList(self):
         for i in range(len(self.guests)):
             self.list = self.guests[i]
             self.chance = random.randint(1, 100)
             if self.chance <= 80:
                 self.guestList.append(self.list)
             else:
                 self.guestRejected.append(self.list)
             self.guestListSTR = ', '.join(self.guestList)
             self.guestRejectedSTR = ', '.join(self.guestRejected)
         print(f'Initial guest list had: {len(self.guests)} names, including: {self.guestsSTR}')
         print(f'After Host decision, from {len(self.guests)} names {len(self.guestList)} left')
         print(f'Host decided on: {self.guestListSTR} to come to the party')
         if len(self.guestRejected) == 1:
             print(f'{self.guestRejectedSTR} was not allowed to the party')
         elif len(self.guestRejected) > 1:
             print(f'{self.guestRejectedSTR} were not allowed to the party')
         else:
             print('There were no guests rejected')


host = Host()
host.add_guests()
host.setGuestList()

# Задание 4:
class Elevator:


    def __init__(self):
        self.currentFloor = 1

    def toFloor(self, floor):

        if floor <= 1 or floor > 16:
            print('There is no such floor')
        else:
            if self.currentFloor > floor:
                for k in range(floor, self.currentFloor + 1):
                    print(f'Elevator is on the floor {self.currentFloor}')
                    self.currentFloor -= 1
            else:
                for k in range(self.currentFloor, floor + 1):
                    print(f'Elevator is on the floor {self.currentFloor}')
                    self.currentFloor += 1
        self.currentFloor = floor
        return self.currentFloor

    def printFloor(self):
        print(f'Elevator is on the floor {self.currentFloor}')

    def onefloorUp(self):
        if self.currentFloor >= 16:
            print('There is no such floor')
        else:
            self.currentFloor += 1

    def onefloorDown(self):
        if self.currentFloor <= 1:
            print('There is no such floor')
        else:
            self.currentFloor -= 1

elevator = Elevator()
elevator.onefloorDown()
elevator.toFloor(12)
elevator.onefloorUp()
elevator.printFloor()
elevator.toFloor(7)
elevator.onefloorDown()
elevator.onefloorDown()
elevator.printFloor()
elevator.toFloor(14)
elevator.onefloorUp()
elevator.onefloorUp()
elevator.printFloor()
elevator.onefloorUp()









