import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


@pytest.fixture
def browser(request):
    driver = webdriver.Chrome()
    driver.maximize_window()

    def teardown():
        driver.quit()

    request.addfinalizer(teardown)

    return driver


# def test_yahoo_search(browser):
#     browser.get('https://www.yahoo.com')
#     assert browser.title == 'Yahoo | Mail, Weather, Search, Politics, News, Finance, Sports & Videos'
#     SEARCHBAR = browser.find_element(By.XPATH, "//input[@name='p']")
#     SEARCHBAR.send_keys(Keys.NUMPAD1, Keys.NUMPAD2, Keys.NUMPAD3)
#     SEARCHBAR.send_keys(Keys.ENTER)


def test_bing_search(browser):
    browser.get('https://www.bing.com')
    assert browser.title == 'Bing'
    SEARCHBAR = browser.find_element('xpath', "//input[@id='sb_form_q']")
    SEARCHBTN = browser.find_element('xpath', "//label[@id='search_icon']")
    SEARCHBAR.click()
    SEARCHBAR.send_keys(123)
    SEARCHBTN.click()


def test_yandex_search(browser):
    browser.get('https://www.ya.ru')
    assert browser.title == 'Яндекс'
    SEARCHBAR = browser.find_element('xpath', "//input[@id='text']")
    SEARCHBAR.click()
    SEARCHBAR.send_keys(123)
    SEARCHBAR.send_keys(Keys.ENTER)
