from datetime import datetime

while True:
    try:
        date_input = input("Введите дату в формате ГГГГ-ММ-ДД: ")
        dateform = datetime.strptime(date_input, '%Y-%m-%d')
        months = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"]
        monthdate = months[dateform.month-1]
        right_date_form = dateform.strftime(f'%d {monthdate} %Y г.')
        if right_date_form:
            print(f'Дата: {right_date_form}')
            break
    except ValueError:
        print("Ошибка: Некорректный формат даты. Пожалуйста, введите дату снова.")
# Добавил строку текста