import math
import random

# Задание 1
# Дан массив:
# var yodaSays = ["on Python", "programming ", "I like "];
# С помощью обращения к элементам массива составьте правильную строку и выведите ее на экран.


yodaSays = ["on Python", "programming ", "I like "]
print (yodaSays[2], yodaSays[1], yodaSays[0])

# Задание 2
# Создать хэш duck, у которого будут ключи name, color и age, назначить им соответствующие значения "Donald", "white", "1".
# Далее, вам необходимо составить с этими свойствами два-три произвольных предложения и вывести их в консоль.
#
# Пример вывода:
# My name is Donald.
# I am 1 year old.
#
# Указания:
# ⦁	Использовать console.log для вывода в консоль.
# ⦁	В этом упражнении вам необходимо использовать символ переноса строки, чтобы объединить предложения в одну строковую переменную.
# ⦁	Вы можете придумать другие дополнительные свойства для хэша duck и вывести более чем 2-3 предложения.

duck = {
    "name": "Donald",
    "color": "white",
    "age": "1"
}
print(f"Hello! My name is {duck['name']}!\nMy body color is {duck['color']}.\nMy favourite number is {duck['age']}.\nThat is also my age.")

# Задание 3
# Напишите программу, которая будет запрашивать логин пользователя, а затем его пароль. Предполагаем, что у нас есть только один пользователь "admin".
#
# Если посетитель вводит "admin", то спрашивать пароль, или ввел пустую строку – выводить "Login canceled", если вводит что-то другое – "Unknown user".
#
# Пароль проверять так. Если введен пароль "BlackOverlord", то выводить "Welcome!", иначе – "Wrong Password!". Если пустая строка - "Login canceled".

name_input = input('Enter your name: ')
if name_input == "admin":
    password_input = input('Enter your password: ')
    if password_input == "BlackOverlord":
        print("Welcome!")
    elif password_input == '':
        print('Login canceled')
    else:
        print("Wrong password!")
elif name_input == '':
    print('Login canceled')
else:
    print('Unknown user')

# Задание 4
# Имеется массив с элементами: ['milk', 'beer', 'beer', 'milk', 'milk']. Необходимо запустить в цикле проверку для каждого элемента массива - когда встречается элемент со значением "milk", в консоль выводить строку "good", если встречается элемент со значением "beer", выводить в консоль строку "bad".
#
# Результат в консоли:
# good
# bad
# bad
# good
# good

drinks = ['milk', 'beer', 'beer', 'milk', 'milk']
for drink in drinks:
    if drink == 'milk':
        print('good')
    elif drink == 'beer':
        print('bad')

# Задание 5
# Необходимо напечатать в консоли с помощью  print  прямоугольник, состоящий из символа,
# который запрошен у пользователя. Прямоугольник должен иметь 20 символов в длину и 7 символов в высоту.

length = 20
width = 7
square = length*width
char = input("Введите знак: ")
for i in range(square):
    if (i+1) % 20 == 0:
        print(char, end='\n')
    else:
        print(char, end='')

# Задание 6
# Напишите программу, которая проверяет ваше знание таблицы умножения.
# Сначала, она должна вывести на экран пример, который должен быть сгенерирован случайным образом, для двух чисел от 1 до 9.
# При этом пользователь должен написать ответ. Если ответ верный, программа должна вывести сообщение "You are correct!".
# Если не верный или пустой, сообщение "You are wrong".

random_integer_1 = math.floor(random.random()*9+1)
random_integer_2 = math.floor(random.random()*9+1)
mult = input(f"{random_integer_1}*{random_integer_2}=")
if int(mult) == (int(random_integer_1)*int(random_integer_2)):
    print("You are correct!")
else:
    print("You are wrong!")




