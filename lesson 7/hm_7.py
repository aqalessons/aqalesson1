# Задание 1
class Car:
    def __init__(self, model, year, color):
        self.model = model
        self.year = year
        self.color = color

    def engine_on(self):
        print(f'Engine of {self.color} {self.model} {self.year} is on')

    def engine_off(self):
        print(f'\nEngine of {self.color} {self.model} {self.year} is off')

    def route(self, kilometers):
        kms = int(kilometers)
        super().__init__()
        Car.engine_on(self)
        for km in range(kms):
            print('vroom', end='')
        Car.engine_off(self)

car = Car('Lada Priora', 2015, 'black')
car.route(14)

# Задание 2
class Person:
    def __init__(self, name, age):
        self.__name = name
        self.__age = age

    def get_name(self):
        super().__init__()
        print(self.__name)

    def get_age(self):
        super().__init__()
        print(self.__age)

person = Person('Jack', 55)
person.get_name()
person.get_age()

# Задание 3
from faker import Faker
fake = Faker()
class Bird:
    def __init__(self, name):
        self.name = name
class Sparrow(Bird):
    def fly(self):
        print(f'{self.name} is flying!')

    def show_type(self):
        print(f'because {self.name} is a sparrow')


class Penguin(Bird):
    def fly(self):
        print(f'{self.name} is not flying!')

    def show_type(self):
        print(f'because {self.name} is a penguin')



sparrow = Sparrow(fake.name())
penguin = Penguin(fake.name())
sparrow2 = Sparrow(fake.name())
penguin2 = Penguin(fake.name())
birds = sparrow, penguin, sparrow2, penguin2

for bird in birds:
    bird.fly()
    bird.show_type()
