# class Dog:
#     def __init__(self, name, age):
#         self.__name = name #приватный атрибут
#         self.age = age
#
#     def bark(self):
#         print(f'{self.__name} is barking!')
#
# my_dog = Dog('Buddy', 3)
# my_dog.bark()

# class MyClass:
#     def __init__(self):
#         self.public_attribute = 10
#         self._protected_attribute = 20
#         self.__private_attribute = 30
#
# obj = MyClass()
# print(obj.public_attribute)
# print(obj._protected_attribute)
# print(obj.__private_attribute)


# class Animal:
#     def __init__(self, name):
#         self.name = name
#
# class Dog(Animal):
#     def bark(self):
#         print(f'{self.name} is barking!')
#
# my_dog = Dog('Charlie')
#
# #
# # class Labrador(Dog):
# #     def fetch(self):
# #         print(f'{self.name} is fetching!')
# #
# #     def bark(self):
# #         super().bark()
# #         print("Labrador-specific bark!")
#
#
# class Cat(Animal):
#     def meow(self):
#         print(f'{self.name} is meowing!')
#
#     def make_sound(animal):
#         animal.bark() if isinstance(animal, Dog) else animal.meow()
#
#
# dog = Dog('Buddy')
# cat = Cat('Whiskers')
#
# animal.make_sound(dog)
# make_sound(cat)

# class Animal:
#     def __init__(self, name):
#         self.name = name
# class Dog(Animal):
#     def make_sound(self):
#         print(f'{self.name} is barking!')
#
#
# class Cat(Animal):
#     def make_sound(self):
#         print(f'{self.name} is meowing!')
#
#
#
# dog = Dog('Buddy')
# cat = Cat('Whiskers')
#
# for animal in (cat, dog):
#     animal.make_sound()

# class BankAccount:
#     def __init__(self, balance=0):
#         self._balance = balance
#
#     def deposit(self, amount):
#         if amount > 0:
#             self._balance += amount
#             print(f'Deposited ${amount}. New balance: ${self._balance}')
#         else:
#             print('Invalid deposit amount')
#
#     def withdraw(self, amount):
#         if 0 < amount <= self._balance:
#             self._balance -= amount
#             print(f"Withdrew ${amount}. New balance: ${self._balance}")
#         else:
#             print('Invalid withdrawal amount of insufficient funds')
#
#     def current_balance(self):
#         return self._balance
#
#
# account = BankAccount(1000)
# account.deposit(500)
# account.withdraw(200)
#
# balance = account.current_balance()
# print(f'Current balance: ${balance}')

# Создайте базовый класс "Транспортное средство" с атрибутами, такими как
# "скорость" и "вместимость" и методом “показать информацию” о данном транспортном средстве.
# Затем создайте два класса-наследника: "Автомобиль" и "Велосипед".
# Каждый из них должен иметь дополнительные уникальные атрибуты и методы, например,
# "тип кузова" для автомобиля и "количество педалей" для велосипеда.

# class TransportVehicle:
#     def __init__(self, speed, capacity):
#         self.speed = speed
#         self.capacity = capacity
#
#     def display_info(self):
#         print(f'Транспортное средство со скоростью {self.speed} км/ч и вместимостью {self.capacity} человек.')
#
#
# class Car(TransportVehicle):
#
#     def __init__(self, speed, capacity, body_type):
#         self.body_type = body_type
#         super().__init__(speed, capacity)
#
#     def display_info(self):
#         super().display_info()
#         print(f'Тип кузова автомобиля: {self.body_type}')
#
#
# car = Car(100,5,"седан")
# car.display_info()








